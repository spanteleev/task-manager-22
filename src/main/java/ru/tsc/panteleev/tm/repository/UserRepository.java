package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(Role.USUAL);
        return add(user);
    }

    @Override
    public User findByLogin(final String login) {
        return models.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst().orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return models.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst().orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public User removeByEmail(final String email) {
        final User user = findByLogin(email);
        if (user == null) return null;
        return remove(user);
    }

}
