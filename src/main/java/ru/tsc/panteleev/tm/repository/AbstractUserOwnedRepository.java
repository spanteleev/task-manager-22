package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;

import java.util.*;
import java.util.stream.Collectors;

public class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @Override
    public M add(final String userId, final M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public List<M> findAll(final String userId) {
        if (userId == null) return Collections.emptyList();
        return models.stream()
                .filter(model -> userId.equals(model.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Comparator<M> comparator) {
        return findAll(userId).stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final String userId, final Sort sort) {
        return findAll(userId, sort.getComparator());
    }

    @Override
    public M findById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        return models.stream()
                .filter(model -> id.equals(model.getId()) && userId.equals(model.getUserId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findByIndex(final String userId, final Integer index) {
        return findAll(userId).get(index);
    }

    @Override
    public M remove(final String userId, final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Override
    public M removeById(final String userId, final String id) {
        if (userId == null || id == null) return null;
        final M model = findById(userId, id);
        return model == null ? null : remove(model);
    }

    @Override
    public M removeByIndex(final String userId, final Integer index) {
        final M model = findByIndex(userId, index);
        return model == null ? null : remove(model);
    }

    @Override
    public void clear(final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public long getSize(final String userId) {
        return models.stream()
                .filter(model -> userId.equals(model.getUserId()))
                .count();
    }
}
