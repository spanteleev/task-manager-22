package ru.tsc.panteleev.tm.repository;

import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.panteleev.tm.exception.field.IndexIncorrectException;
import ru.tsc.panteleev.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> models = new ArrayList<>();

    @Override
    public M add(final M model) {
        models.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return models;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return models.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public List<M> findAll(final Sort sort) {
        return findAll(sort.getComparator());
    }

    @Override
    public M findById(final String id) {
        return models.stream()
                .filter(model -> id.equals(model.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public M findByIndex(final Integer index) {
        if (index > getSize()) throw new IndexIncorrectException();
        return models.get(index);
    }

    @Override
    public M remove(final M model) {
        models.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findById(id);
        return model == null ? null : remove(model);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findByIndex(index);
        return model == null ? null : remove(model);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        models.removeAll(collection);
    }

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public long getSize() {
        return models.size();
    }

}
