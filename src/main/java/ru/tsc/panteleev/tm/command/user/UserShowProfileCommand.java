package ru.tsc.panteleev.tm.command.user;

import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.entity.UserNotFoundException;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class UserShowProfileCommand extends AbstractUserCommand {

    public static final String NAME = "user-show-profile";

    public static final String DESCRIPTION = "Show user profile.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER SHOW PROFILE]");
        final User user = getAuthService().getUser();
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
