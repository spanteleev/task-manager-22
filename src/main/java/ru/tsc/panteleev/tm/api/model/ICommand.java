package ru.tsc.panteleev.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();
}
