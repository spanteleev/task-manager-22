package ru.tsc.panteleev.tm.api.service;

import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
