package ru.tsc.panteleev.tm.service;

import ru.tsc.panteleev.tm.api.service.IAuthService;
import ru.tsc.panteleev.tm.api.service.IUserService;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.field.LoginEmptyException;
import ru.tsc.panteleev.tm.exception.field.PasswordEmptyException;
import ru.tsc.panteleev.tm.exception.user.AccessDeniedException;
import ru.tsc.panteleev.tm.exception.user.LoginOrPasswordIncorrectException;
import ru.tsc.panteleev.tm.exception.user.PermissionException;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new LoginOrPasswordIncorrectException();
        if (user.isLocked()) throw new LoginOrPasswordIncorrectException();
        if (!user.getPasswordHash().equals(HashUtil.salt(password))) throw new LoginOrPasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public User registry(final String login, final String password, final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public User getUser() {
        return userService.findById(getUserId());
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void checkRoles(final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role userRole = user.getRole();
        if (userRole == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(userRole);
        if (!hasRole) throw new PermissionException();
    }
}
